class rifle:
  def __init__(self, f):
        temp = ''
        d, di, dl = open(f).read(), {}, []
        for l in d.split('\n'):
            try:
                l[0]
            except:
                continue
            if l[0] == '(':
                temp = ''
                for c in l:
                    if c == ')':
                        dl.append(temp[1:])
                        break
                    temp += c
        for df in dl:
            tmp = ''
            for c in df.replace(' ', ''):
                tmp += c
                if '->' in tmp:
                    di[tmp.split('->')[0]] = tmp.split('->')[1]
                elif ':' in tmp:
                    di[tmp.split(':')[0]] = tmp.split(':')[1]
        for k in di:
            if di[k].isdecimal():
                di[k] = int(di[k])
                continue
            for ei in '+-/*^':
                for var in di:
                    if var in di[k]:
                        di[k] = di[k].replace(str(var), str(di[var]))
                if (str(ei) in str(di[k])):
                    di[k] = eval(str(di[k]))
                    break
        print(di)
